#ifndef BRICKS_H
#define BRICKS_H
#include "raylib.h"
#include "screens.h"
#define FIL 5
#define COL 20
namespace Arkanoid 
{

	namespace bricks 
	{
		enum BrickType
		{
			empty,
			normal,
			indestructible
		};
		struct Bricks
		{
			Texture2D greyBrick;
			Texture2D yellowBrick;
			BrickType brickType;
			Vector2 position;
			bool active;
			int totalBricks;
		};
		extern Vector2 brickSize;
		extern Vector2 positionVec2Cast;
		extern Bricks bricksArray[FIL][COL];
		extern Bricks bricks;
		extern int initialYPos;
		void init();
		void update();
		void draw();
		void deInit();
		void loadTexture();
		void unLoadTexture();

	}
}
#endif // !BRICKS_H

