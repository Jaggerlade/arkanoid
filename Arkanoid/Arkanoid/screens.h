#ifndef SCREENS_H
#define SCREENS_H
#include <iostream>
#include "raylib.h"
#include "Player.h"
#include "bricks.h"
#include "game_loop.h"
namespace Arkanoid 
{
	namespace screens 	
	{
		enum STATES
		{
			playing,
			menu,
			config,
			defeat,
			victory
		};
		enum LEVELS
		{
			level1,
			level2,
			level3
		};
		extern Texture2D backgroundTexture;
		extern STATES states;
		void init();
		void update();
		void draw();
		void deInit();
		void reInit();
		void loadTexture();
		void unLoadTexture();

	}
}
#endif // !SCREENS_H

