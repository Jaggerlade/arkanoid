#ifndef GAME_LOOP
#define GAME_LOOP
#include "bricks.h"
#include "player.h"
#include "ball.h"
#include "screens.h"
namespace Arkanoid
{
	namespace game_loop
	{
		void init();
		void update();
		void draw();
		void deInit();
		void game();
		void reInit();
	}
}
#endif // !GAME_LOOP

