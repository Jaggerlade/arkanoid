#include "game_loop.h"
namespace Arkanoid 
{
	namespace game_loop 
	{

		void init()
		{
			SetTargetFPS(120);
			screens::init();
			bricks::init();
			player::init();
			ball::init();
		}
		void update()
		{
			screens::update();
			player::update();
			ball::update();
			bricks::update();
		}
		void draw()
		{
			ClearBackground(WHITE);
			BeginDrawing();
			screens::draw();
			player::draw();
			bricks::draw();
			ball::draw();
			EndDrawing();
		}
		void deInit()
		{
			player::deInit();
			screens::deInit();
		}
		void game() 
		{
			update();
			draw();

		}
		void reInit() 
		{
			bricks::init();
			player::init();
			ball::init();
		}
	}

}