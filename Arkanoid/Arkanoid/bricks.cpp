#include <iostream>
#include "bricks.h"
using namespace std;
namespace Arkanoid 
{
	namespace bricks 
	{
		Vector2 brickSize;
		Vector2 positionVec2Cast;
		Bricks bricksArray[FIL][COL];
		Bricks bricks;
		int initialYPos;
		static int cantUnbreak = 4;
		static int cantEmpty = 4;
		static int cantMin;
		static int randI;
		static int randJ;
		static int loadedTextureGrey = 0;
		static int loadedTextureYellow = 0;
		void init()
		{
			cantMin = 0;
			bricks.totalBricks =(COL*FIL) - (cantEmpty + cantUnbreak);
			brickSize = { static_cast<float>(GetScreenWidth() / COL),40.0 };
			int initialYPos = 30;
	
			for (int i = 0; i < FIL; i++)
			{
				for (int j = 0; j < COL; j++)
				{
					positionVec2Cast = { static_cast<float>(j * brickSize.x + brickSize.x / 2),i*brickSize.y + initialYPos };
					bricksArray[i][j].position = (Vector2)positionVec2Cast;
					bricksArray[i][j].active = true;
					bricksArray[i][j].brickType = normal;
				}
			}
			do
			{
				randI = GetRandomValue(0, FIL);
				randJ = GetRandomValue(0, COL);
				if (bricksArray[randI][randJ].brickType == normal)
				{
					bricksArray[randI][randJ].brickType = indestructible;
					cantMin++;
				}
			} while (cantMin < cantUnbreak);
			cantMin = 0;
			do
			{
				randI = GetRandomValue(0, FIL);
				randJ = GetRandomValue(0, COL);
				if (bricksArray[randI][randJ].brickType == normal)
				{
					bricksArray[randI][randJ].brickType = empty;
					cantMin++;
				}
			} while (cantMin < cantEmpty);
			if (loadedTextureGrey == 0 && loadedTextureYellow == 0)
			{
				loadTexture();

			}

		}
		void update()
		{
		}
		void draw()
		{
			
			if (screens::states == screens::playing)
			{
				for (int i = 0; i < FIL; i++)
				{
					for (int j = 0; j < COL; j++)
					{
						if (bricksArray[i][j].active && bricksArray[i][j].brickType != empty)
						{
							if ((i + j) % 2 == 0)
							{
								if (bricksArray[i][j].brickType == normal)
								{
									DrawTexture(bricks.yellowBrick, bricksArray[i][j].position.x - brickSize.x / 2, bricksArray[i][j].position.y - brickSize.y / 2, WHITE);
									//DrawRectangle(bricksArray[i][j].position.x - brickSize.x / 2, bricksArray[i][j].position.y - brickSize.y / 2, brickSize.x, brickSize.y, GRAY);
								}
								else
								{
									DrawRectangle(bricksArray[i][j].position.x - brickSize.x / 2, bricksArray[i][j].position.y - brickSize.y / 2, brickSize.x, brickSize.y, BLUE);
								}
								
							}
							else if((i + j) % 2 != 0)
							{
								if (bricksArray[i][j].brickType == normal)
								{
									DrawTexture(bricks.greyBrick, bricksArray[i][j].position.x - brickSize.x / 2, bricksArray[i][j].position.y - brickSize.y / 2, WHITE);
									//DrawRectangle(bricksArray[i][j].position.x - brickSize.x / 2, bricksArray[i][j].position.y - brickSize.y / 2, brickSize.x, brickSize.y, DARKGRAY);
								}
								else
								{
									DrawRectangle(bricksArray[i][j].position.x - brickSize.x / 2, bricksArray[i][j].position.y - brickSize.y / 2, brickSize.x, brickSize.y, BLUE);
								}
								
								
							}
						
						}

					}
				}
			}
			
		}
		void deInit()
		{
			unLoadTexture();
		}
		void loadTexture()
		{
			Image reSize;
			reSize = LoadImage("res/Bricks-Player/brickGray.png");
			ImageResize(&reSize, bricks::brickSize.x, bricks::brickSize.y);
			bricks.greyBrick = LoadTextureFromImage(reSize);
			UnloadImage(reSize);
			loadedTextureGrey++;

			reSize = LoadImage("res/Bricks-Player/brickYellow.png");
			ImageResize(&reSize, bricks::brickSize.x, bricks::brickSize.y);
			bricks.yellowBrick = LoadTextureFromImage(reSize);
			UnloadImage(reSize);
			loadedTextureYellow++;
		}
		void unLoadTexture()
		{
			UnloadTexture(bricks.greyBrick);
			UnloadTexture(bricks.yellowBrick);
		}
	}
}