#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "screens.h"

#define MAXPLAYERLIVES 5
namespace Arkanoid 
{

	namespace player 
	{
		struct Players
		{
			Rectangle rectangle;
			int playerScore;
			int playerLives;
			Texture2D playerTexture;
		};
		extern Players player;
		void init();
		void update();
		void draw();
		void deInit();
		void playerMovement();
		void loadTexture();
		void unLoadTexture();
	}

}
#endif // !PLAYER_H
