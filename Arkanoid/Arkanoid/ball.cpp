#include "ball.h"
namespace Arkanoid
{
	namespace ball
	{
		Ball ball;
		void init()
		{
			ball.radius = static_cast<float> (GetScreenWidth()/120);
			ball.position = {static_cast<float> (GetScreenWidth() / 2),static_cast<float>(GetScreenHeight() * 7 / 8 - 30 )};
			ball.speed = { 0,0 };
			ball.active = false;
		}
		void update()
		{
			launch();
			movement();
			onCollision();
		}
		void draw()
		{
			if (screens::states == screens::playing)
			{
				DrawCircleV(ball.position, ball.radius, RED);
			}
			
		}
		void deInit()
		{

		}
		void launch() 
		{
			if (!ball.active)
			{
				if (IsKeyPressed(KEY_SPACE))
				{
					ball.active = true;
					ball.speed = { 0.0f, -5.0f };
				}
			}
		}
		void movement() 
		{
			if (ball.active)
			{
				ball.position.x += ball.speed.x;
				ball.position.y += ball.speed.y;
			}
			else
			{
				ball.position = { player::player.rectangle.x+player::player.rectangle.width/2, static_cast<float>( GetScreenHeight() * 7 / 8 )- 30 };
			}

		}
		void onCollision() 
		{
			//ball vs wall
			if (ball.position.x + ball.radius >= GetScreenWidth() || ball.position.x - ball.radius <= 0)
			{
				ball.speed.x *= -1;
			}
			if (ball.position.y - ball.radius <= 0) { ball.speed.y *= -1; }
			if (ball.position.y + ball.radius >= GetScreenHeight())
			{
				ball.speed = { 0.0f,0.0f };
				ball.active = false;
				player::player.playerLives--;
			}
			//ball vs player
			
			if (CheckCollisionCircleRec(ball.position,ball.radius,player::player.rectangle))
			{
				ball.speed.y *= -1;
				ball.speed.x = (ball.position.x - (player::player.rectangle.x + player::player.rectangle.width / 2) ) / 20;
			}

			for (int i = 0; i < FIL; i++)
			{
				for (int j = 0; j < COL; j++)
				{
					if (bricks::bricksArray[i][j].active)
					{
						// Hit below
						if (((ball.position.y - ball.radius) <= (bricks::bricksArray[i][j].position.y + bricks::brickSize.y / 2)) &&
							((ball.position.y - ball.radius) > (bricks::bricksArray[i][j].position.y + bricks::brickSize.y / 2 + ball.speed.y)) &&
							((fabs(ball.position.x - bricks::bricksArray[i][j].position.x)) < (bricks::brickSize.x / 2 + ball.radius * 2 / 3)) && (ball.speed.y < 0))
						{
							if (bricks::bricksArray[i][j].brickType == bricks::normal)
							{
								bricks::bricksArray[i][j].active = false;
								ball.speed.y *= -1;
								player::player.playerScore += 300;
								bricks::bricks.totalBricks--;
							}
							else if (bricks::bricksArray[i][j].brickType == bricks::indestructible)
							{
								ball.speed.y *= -1;
							}
							
						}
						// Hit above
						if (((ball.position.y + ball.radius) >= (bricks::bricksArray[i][j].position.y - bricks::brickSize.y / 2)) &&
							((ball.position.y + ball.radius) < (bricks::bricksArray[i][j].position.y - bricks::brickSize.y / 2 + ball.speed.y)) &&
							((fabs(ball.position.x - bricks::bricksArray[i][j].position.x)) < (bricks::brickSize.x / 2 + ball.radius * 2 / 3)) && (ball.speed.y > 0))
						{
							if (bricks::bricksArray[i][j].brickType == bricks::normal)
							{
								bricks::bricksArray[i][j].active = false;
								player::player.playerScore += 300;
								ball.speed.y *= -1;
								bricks::bricks.totalBricks--;
							}
							else if (bricks::bricksArray[i][j].brickType == bricks::indestructible)
							{
								ball.speed.y *= -1;
							}
						}
						// Hit left
						if (((ball.position.x + ball.radius) >= (bricks::bricksArray[i][j].position.x - bricks::brickSize.x / 2)) &&
							((ball.position.x + ball.radius) < (bricks::bricksArray[i][j].position.x - bricks::brickSize.x / 2 + ball.speed.x)) &&
							((fabs(ball.position.y - bricks::bricksArray[i][j].position.y)) < (bricks::brickSize.y / 2 + ball.radius * 2 / 3)) && (ball.speed.x > 0))
						{
							if (bricks::bricksArray[i][j].brickType == bricks::normal)
							{
								bricks::bricksArray[i][j].active = false;
								player::player.playerScore += 300;
								ball.speed.x *= -1;
								bricks::bricks.totalBricks--;
							}
							else if (bricks::bricksArray[i][j].brickType == bricks::indestructible)
							{
								ball.speed.x *= -1;
							}
						}
						// Hit right
						if (((ball.position.x - ball.radius) <= (bricks::bricksArray[i][j].position.x + bricks::brickSize.x / 2)) &&
							((ball.position.x - ball.radius) > (bricks::bricksArray[i][j].position.x + bricks::brickSize.x / 2 + ball.speed.x)) &&
							((fabs(ball.position.y - bricks::bricksArray[i][j].position.y)) < (bricks::brickSize.y / 2 + ball.radius * 2 / 3)) && (ball.speed.x < 0))
						{
							if (bricks::bricksArray[i][j].brickType == bricks::normal)
							{
								bricks::bricksArray[i][j].active = false;
								player::player.playerScore += 300;
								ball.speed.x *= -1;
								bricks::bricks.totalBricks--;
							}
							else if (bricks::bricksArray[i][j].brickType == bricks::indestructible)
							{
								ball.speed.x *= -1;
							}
						}
					}
				}
			}
		}
	}
}
