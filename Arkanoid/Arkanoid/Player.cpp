#include "player.h"
using namespace std;
namespace Arkanoid 
{
	
	namespace player 
	{
		Players player;
		static int loadedTexture = 0;
		void init()
		{
			player.rectangle = { static_cast<float>(GetScreenWidth() / 2), static_cast<float>(GetScreenHeight()* 7/8),
								 static_cast<float> (GetScreenWidth()/8.5f),static_cast<float> (GetScreenHeight()/ 25) };
			player.playerScore = 0;
			player.playerLives = MAXPLAYERLIVES;
			if (loadedTexture == 0)
			{
				loadTexture();

			}
			
		}
		void update()
		{
			playerMovement();
		}
		void draw()
		{
			if (screens::states == screens::playing)
			{
				DrawTexture(player.playerTexture, player.rectangle.x, player.rectangle.y, WHITE);	
				for (int i = 0; i < player.playerLives; i++) DrawRectangle(20 + 40 * i, GetScreenHeight() - 30, 35, 10, LIGHTGRAY);
			}
		}
		void deInit()
		{
			unLoadTexture();
		}
		void playerMovement() 
		{
			if (IsKeyDown(KEY_A) && player.rectangle.x > 0) { player.rectangle.x -= 500*GetFrameTime(); }
			if (IsKeyDown(KEY_D)&& player.rectangle.x <= GetScreenWidth()-player.rectangle.width) { player.rectangle.x += 500*GetFrameTime(); }
		}
		void loadTexture()
		{
			Image reSize;
			reSize = LoadImage("res/Bricks-Player/paddle.png");
			ImageResize(&reSize,player.rectangle.width,player.rectangle.height);
			player.playerTexture = LoadTextureFromImage(reSize);
			UnloadImage(reSize);
			loadedTexture++;
		}
		void unLoadTexture()
		{
			UnloadTexture(player.playerTexture);
			
		}
	}
}
