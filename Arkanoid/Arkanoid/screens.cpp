#include "screens.h"
using namespace std;
namespace Arkanoid 
{
	namespace screens 
	{
		Texture2D backgroundTexture;
		STATES states;
		static int key;
		static bool defeatBool = false;
		static bool victoryBool = false;
		static int loadedTexture = 0;
		void init() 
		{
			states = menu;
			if (loadedTexture == 0)
			{
				loadTexture();
			}
		}
		void update() 
		{

			if (states == menu)
			{
				if (IsKeyPressed(KEY_ENTER))
				{
					states = playing;
					game_loop::reInit();
				}
				else if (IsKeyPressed(KEY_C))
				{
					states = config;
				}
				//cout << (char)GetKeyPressed()<<endl;
			}
			
			if (player::player.playerLives <= 0 && bricks::bricks.totalBricks > 0 && !defeatBool)
			{
				
				states = defeat;
				if (IsKeyPressed(KEY_M))
				{
					states = menu;
					defeatBool = true;
				}

			}
			else if (bricks::bricks.totalBricks <= 0 && player::player.playerLives > 0 && !victoryBool)
			{
				states = victory;
				if (IsKeyPressed(KEY_M))
				{
					states = menu;
					victoryBool = true;
				}
			}	
		}
		void draw() 
		{
			if (states == menu)
			{
				DrawText("ARKANOID", GetScreenWidth() / 2, GetScreenHeight() / 4, GetScreenWidth()/30, RED);
				DrawText("Play[Press Enter]", GetScreenWidth() / 2, GetScreenHeight() *4/8, GetScreenWidth()/40, GREEN);
				DrawText("Config[Press C]", GetScreenWidth() /2, GetScreenHeight() * 5/8, GetScreenWidth() / 40, GREEN);
				DrawText("Exit[Press ESC]", GetScreenWidth() / 2, GetScreenHeight()*6/8, GetScreenWidth() / 40, GREEN);
			}
			else if (states == playing)
			{
				DrawTexture(backgroundTexture, -150, -150, WHITE);
				DrawText(TextFormat("SCORE: %i", player::player.playerScore), GetScreenWidth() *6/8, GetScreenHeight() * 5 / 8, GetScreenWidth() / 40, GREEN);
			}
			else if (states == defeat)
			{
				DrawText("You lose!", GetScreenWidth() / 2, GetScreenHeight() / 4, GetScreenWidth() / 30, RED);
				DrawText("Menu[M]", GetScreenWidth() / 2, GetScreenHeight() * 4 / 8, GetScreenWidth() / 40, GREEN);
				DrawText("Exit[Press ESC]", GetScreenWidth() / 2, GetScreenHeight() * 5 / 8, GetScreenWidth() / 40, GREEN);
			}
			else if (states == victory)
			{
				DrawText("You win!", GetScreenWidth() / 2, GetScreenHeight() / 4, GetScreenWidth() / 30, RED);
				DrawText("Menu[M]", GetScreenWidth() / 2, GetScreenHeight() * 4 / 8, GetScreenWidth() / 40, GREEN);
				DrawText("Exit[Press ESC]", GetScreenWidth() / 2, GetScreenHeight() * 5 / 8, GetScreenWidth() / 40, GREEN);
			}
			
		}
		void deInit() 
		{
			unLoadTexture();
		}
		void loadTexture()
		{
			Image reSize;
			reSize = LoadImage("res/BackGround/backGround1.png");
			ImageResize(&reSize, GetScreenWidth() + 300, GetScreenHeight()+150);
			backgroundTexture = LoadTextureFromImage(reSize);
			UnloadImage(reSize);
			loadedTexture++;
		}
		void unLoadTexture()
		{
			UnloadTexture(backgroundTexture);
		}
	}
}