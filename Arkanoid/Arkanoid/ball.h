#ifndef BALL_H
#define BALL_H
#include "raylib.h"
#include "Player.h"
#include "bricks.h"
#include "screens.h"
#include <iostream>
namespace Arkanoid 
{
	namespace ball 
	{
		struct Ball
		{
			Vector2 position;
			Vector2 speed;
			int radius;
			bool active;
		};
		extern Ball ball;
		void init();
		void update();
		void draw();
		void deInit();
		void launch();
		void movement();
		void onCollision();
	}

}
#endif // !BALL_H

